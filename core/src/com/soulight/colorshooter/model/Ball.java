package com.soulight.colorshooter.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Circle;

public class Ball {

	private Circle circle;
	private BallPosition ballPosition;
	private Color color;
	
	public Ball(){}
	
	public Ball(Circle circle, BallPosition ballPosition, Color color) {
		this.circle = circle;
		this.ballPosition = ballPosition;
		this.color = color;
	}
	
	public Circle getCircle() {
		return circle;
	}
	
	public void setCircle(Circle circle) {
		this.circle = circle;
	}

	public BallPosition getBallPosition() {
		return ballPosition;
	}

	public void setBallPosition(BallPosition ballPosition) {
		this.ballPosition = ballPosition;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
}
