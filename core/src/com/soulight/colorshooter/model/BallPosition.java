package com.soulight.colorshooter.model;

public enum BallPosition {
	UP, DOWN, LEFT, RIGHT
}
