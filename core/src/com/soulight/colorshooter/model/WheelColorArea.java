package com.soulight.colorshooter.model;

import com.badlogic.gdx.graphics.Color;

public class WheelColorArea {

	private float startDegree;
	private float endDegree;
	private Color color;
	
	public WheelColorArea() {}
	
	public WheelColorArea(float startDegree, float endDegree, Color color) {
		super();
		this.startDegree = startDegree;
		this.endDegree = endDegree;
		this.color = color;
	}
	
	public float getStartDegree() {
		return startDegree;
	}

	public void setStartDegree(float startDegree) {
		this.startDegree = startDegree;
	}

	public float getEndDegree() {
		return endDegree;
	}

	public void setEndDegree(float endDegree) {
		this.endDegree = endDegree;
	}

	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	
}
