package com.soulight.colorshooter.screen;

import com.badlogic.gdx.Screen;
import com.soulight.colorshooter.ColorShooter;

public class BaseScreen implements Screen{

	ColorShooter game;
	
	public BaseScreen(ColorShooter game) {
		this.game = game;
		this.game.getActionResolver().showAds();
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
