package com.soulight.colorshooter.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.soulight.colorshooter.ColorShooter;
import com.soulight.colorshooter.Constants;
import com.soulight.colorshooter.resource.Assets;

public class MenuScreen extends BaseScreen {
	
	private Button menuPlayButton, menuShareButton, menuRateButton, menuLeaderBoardButton;
	
	public MenuScreen(final ColorShooter game) {
		super(game);
		generateButton();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	
		Assets.menuScreenStage.act();
		Assets.menuScreenStage.draw();
	}
	
	public void generateButton() {
		menuPlayButton = new Button(new TextureRegionDrawable(Assets.menuPlayUp),new TextureRegionDrawable(Assets.menuPlayDown));
		menuPlayButton.setSize(Assets.rectangleSizeMap.get("menuPlayUp").getWidth() * Constants.SCREEN_RATIO, Assets.rectangleSizeMap.get("menuPlayUp").getHeight() * Constants.SCREEN_RATIO);
		menuPlayButton.setPosition((Gdx.graphics.getWidth()/2) - (menuPlayButton.getWidth() / 2), (Gdx.graphics.getHeight() / 2) - (menuPlayButton.getHeight()/2));
		menuPlayButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreen(new GameScreen(game));
			}
		});
		
		menuRateButton = new Button(new TextureRegionDrawable(Assets.menuRateUp), new TextureRegionDrawable(Assets.menuRateDown));
		menuRateButton.setSize(Assets.rectangleSizeMap.get("menuRateUp").getWidth() * Constants.SCREEN_RATIO, Assets.rectangleSizeMap.get("menuPlayUp").getHeight() * Constants.SCREEN_RATIO);
		menuRateButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (!game.getActionResolver().isSignedIn()) game.getActionResolver().login();
				game.getActionResolver().rate();
			}
		});
		
		menuShareButton = new Button(new TextureRegionDrawable(Assets.menuShareUp), new TextureRegionDrawable(Assets.menuShareDown));
		menuShareButton.setSize(Assets.rectangleSizeMap.get("menuShareUp").getWidth() * Constants.SCREEN_RATIO, Assets.rectangleSizeMap.get("menuShareUp").getHeight() * Constants.SCREEN_RATIO);
		menuShareButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (!game.getActionResolver().isSignedIn()) game.getActionResolver().login();
				game.getActionResolver().share();
			}
		});
		
		menuLeaderBoardButton = new Button(new TextureRegionDrawable(Assets.menuLeaderBoardUp), new TextureRegionDrawable(Assets.menuLeaderBoardDown));
		menuLeaderBoardButton.setSize(Assets.rectangleSizeMap.get("menuLeaderBoardUp").getWidth() * Constants.SCREEN_RATIO, Assets.rectangleSizeMap.get("menuLeaderBoardUp").getHeight() * Constants.SCREEN_RATIO);
		menuLeaderBoardButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (!game.getActionResolver().isSignedIn()) game.getActionResolver().login();
				game.getActionResolver().getLeaderBoard();
			}
		});
		
		Assets.menuScreenStage.addActor(menuPlayButton);
		Assets.menuScreenStage.addActor(menuRateButton);
		Assets.menuScreenStage.addActor(menuShareButton);
		Assets.menuScreenStage.addActor(menuLeaderBoardButton);
		
		Gdx.input.setInputProcessor(Assets.menuScreenStage);
	}

}