package com.soulight.colorshooter.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.soulight.colorshooter.ColorShooter;
import com.soulight.colorshooter.gamerunner.GameRenderer;
import com.soulight.colorshooter.gamerunner.GameWorld;

public class GameScreen extends BaseScreen{
	
	private GameWorld gameWorld;
	private GameRenderer renderer;
	
	public GameScreen(ColorShooter game) {
		super(game);
		
		gameWorld = new GameWorld();
		gameWorld.setGame(game);
		renderer = new GameRenderer(gameWorld);
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		gameWorld.update(delta);
		renderer.render(delta);
	}
	
}
