package com.soulight.colorshooter.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.soulight.colorshooter.Constants;
import com.soulight.colorshooter.helper.FileHelper;
import com.soulight.colorshooter.model.RectangleSize;

public class Assets {

	public static Texture texture;
	
	public static Stage menuScreenStage, gameScreenStage, overScreenStage, pauseScreenStage;
	
//	public static Rectangle pauseMenu, pauseResume, pauseRestart;
//	public static Rectangle overMenu, overRestart;
//	public static Rectangle gamePause;
	
	public static TextureRegion menuScreen, gameScreen, overScreen, pauseScreen;
	
	public static TextureRegion menuPlayUp, menuPlayDown, menuLeaderBoardUp, menuLeaderBoardDown, menuShareUp, menuShareDown, menuRateUp, menuRateDown;
	public static TextureRegion pauseMenuUp, pauseMenuDown, pauseResumeUp, pauseResumeDown, pauseRestartUp, pauseRestartDown;
	public static TextureRegion overMenuUp, overMenuDown, overRestartUp, overRestartDown;
	public static TextureRegion gamePauseUp, gamePauseDown;
	
	public static Skin skin;

	public static Map<String,Rectangle> rectangleRegionMap = new HashMap<String,Rectangle>();
	public static Map<String,RectangleSize> rectangleSizeMap = new HashMap<String,RectangleSize>();
	
	private static Preferences prefs;
	
	public static void loadBase() {
		if (skin == null) {
			skin = new Skin(Gdx.files.internal(Constants.SKIN_PATH));
		}
		
		prefs = Gdx.app.getPreferences(Constants.APP_NAME);

		if (!prefs.contains("highScore")) {
			prefs.putInteger("highScore", 0);
		}
		
		texture = new Texture(Gdx.files.internal(Constants.TEXTURE_IMAGE_PATH));
		generateRectangleDataMap();

	}
	
	public static void loadMainScreenAsset() {	
		menuScreenStage = new Stage();
		menuPlayUp = generateRegion("menuPlayUp");
		menuPlayDown = generateRegion("menuPlayDown");
		
	}
	
	public static void loadGameScreenAsset() {
		gameScreenStage = new Stage();
		overScreenStage = new Stage();
		pauseScreenStage = new Stage();
		
		gamePauseUp = generateRegion("gamePauseUp");
		gamePauseDown = generateRegion("gamePauseDown");
		
	}
	
	public static void load() {
		loadBase();
		loadMainScreenAsset();
		loadGameScreenAsset();	
	}
	
	public static void dispose() {
		menuScreenStage.dispose();
		gameScreenStage.dispose(); 
		overScreenStage.dispose(); 
		pauseScreenStage.dispose();
		skin.dispose();
	}
	
	public static void setHighScore(int val) {
		prefs.putInteger("highScore", val);
		prefs.flush();
	}

	public static int getHighScore() {
		return prefs.getInteger("highScore");
	}
	
	private static TextureRegion generateRegion(String regionKey) {
		Rectangle rectangle = rectangleRegionMap.get(regionKey);
		TextureRegion textureRegion = new TextureRegion(texture,(int)rectangle.x,(int)rectangle.y,(int)rectangle.width,(int)rectangle.height);
		return textureRegion;
	}
	
	private static void generateRectangleDataMap() {
		List<String> lines = FileHelper.readFileContentByLines(Constants.TEXTURE_DATA_PATH,Constants.LINEBREAK_SEPARATOR);
		for (String line : lines) {
			try {
				String[] keyValue = line.split(Constants.COLON_SEPARATOR);
				String key = keyValue[0];
				String[] region = keyValue[1].split(Constants.COMMA_SEPARATOR);
				String[] size = keyValue[2].split(Constants.COMMA_SEPARATOR);
				rectangleRegionMap.put(key, new Rectangle(Integer.parseInt(region[0]),Integer.parseInt(region[1]),Integer.parseInt(region[2]),Integer.parseInt(region[3])));
				rectangleSizeMap.put(key, new RectangleSize(Float.parseFloat(size[0]),Float.parseFloat(size[1])));
			} catch (Exception e) {
				Gdx.app.error(Constants.APP_NAME, "Failed to retrieve the region data", e);
			}
		}
	}
}