package com.soulight.colorshooter;

import com.badlogic.gdx.Game;
import com.soulight.colorshooter.resource.Assets;
import com.soulight.colorshooter.screen.MenuScreen;


public class ColorShooter extends Game {
	
	private ActionResolver actionResolver;
	
	public ColorShooter(ActionResolver actionResolver) {
		super();
		this.actionResolver = actionResolver;
	}

	@Override
	public void create() {		
		Assets.load();
		this.setScreen(new MenuScreen(this));
	}

	@Override
	public void dispose() {
		super.dispose();
		Assets.dispose();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	public ActionResolver getActionResolver() {
		return actionResolver;
	}
}

