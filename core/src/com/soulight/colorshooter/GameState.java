package com.soulight.colorshooter;

public enum GameState {
	PAUSE, OVER, RUNNING;
}
