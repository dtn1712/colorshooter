package com.soulight.colorshooter.helper;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
 
public class InputHelper {
	
	public static boolean isButtonTouched(List<Button> buttons) {
		for (Button button : buttons) {
			Rectangle r = new Rectangle(button.getX(),button.getY(),button.getWidth(),button.getHeight());
			System.out.println(r);
			if (LogicHelper.pointInRectangle(r, (float)Gdx.input.getX(),(float)(Gdx.graphics.getHeight() - Gdx.input.getY()))) {
				return true;
			}
		}
		return false;
	}
	
}
