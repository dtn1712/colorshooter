package com.soulight.colorshooter.helper;

import com.badlogic.gdx.graphics.Color;
import com.soulight.colorshooter.Constants;

public class ColorHelper {

	public static Color rgb(int r, int g, int b){
		return new Color(r / 255f, g / 255f, b / 255f, 1.0f);
	}
	
	public static Color bright(Color c){
		return c.lerp(1f, 1f, 1f, 1f, 0.5f);
	}
	
	public static Color getRandomColor() {
		return Constants.COLORS_SET.get((int) (Math.random() * Constants.COLORS_SET.size()));
	}
	
}
