package com.soulight.colorshooter.helper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.soulight.colorshooter.Constants;

public class FontHelper {

	public static BitmapFont generateFont(String fontFilePath, int fontSize) {
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(fontFilePath));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = fontSize;
		BitmapFont font = generator.generateFont(parameter);
		generator.dispose();
		return font;
	}
	
	public static BitmapFont generateFont(String fontFilePath) {
		int fontSize = Constants.DEFAULT_FONT_SIZE;
		double ratio = (double) Gdx.graphics.getWidth() / (double) Gdx.graphics.getHeight(); 
		if (ratio > 0.5 && ratio < 0.6) {
			fontSize = Constants.SMALL_SCREEN_FONT_SIZE;
		} else if (ratio > 0.6 && ratio < 1.0) {
			fontSize = Constants.BIG_SCREEN_FONT_SIZE;
		}
		return generateFont(fontFilePath,fontSize);
	}
}
