package com.soulight.colorshooter.helper;

import java.util.Arrays;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class FileHelper {

	public static List<String> readFileContentByLines(String filePath, String separator) {
		FileHandle fileData = Gdx.files.internal(filePath);
		String dataContent = fileData.readString();
		List<String> lines = Arrays.asList(dataContent.split(separator));
		return lines;
	}
}
