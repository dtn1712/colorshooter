package com.soulight.colorshooter.gamerunner;

import java.util.Map;
import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;
import com.soulight.colorshooter.Constants;
import com.soulight.colorshooter.GameState;
import com.soulight.colorshooter.helper.FontHelper;
import com.soulight.colorshooter.model.WheelColorArea;
import com.soulight.colorshooter.resource.Assets;

public class GameRenderer {
	
	private GameWorld gameWorld;
	private OrthographicCamera cam;
	private ShapeRenderer shapeRenderer;
	private SpriteBatch batch;
	
	private Circle wheel;
	private Map<Color,WheelColorArea> colorAreaMap;
	
	private BitmapFont font;
	
	public GameRenderer(GameWorld gameWorld) {
		this.gameWorld = gameWorld;
		
        cam = new OrthographicCamera(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0);
        cam.update();
        
        batch = new SpriteBatch();
		batch.setProjectionMatrix(cam.combined);
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setProjectionMatrix(cam.combined);

		initGameObjects();
		initAssets();
     
	}
	
	private void initGameObjects() {
		wheel = gameWorld.getWheel();
		colorAreaMap = gameWorld.getColorAreaMap();
	}
	
	private void initAssets() {
		font = FontHelper.generateFont(Constants.FONT_PATH);
	}
	
	private void drawWheel() {
		shapeRenderer.begin(ShapeType.Filled);
		for (Entry<Color, WheelColorArea> entry : colorAreaMap.entrySet()) {
			shapeRenderer.setColor(entry.getKey());
			shapeRenderer.arc(wheel.x,wheel.y, Constants.WHEEL_RADIUS, entry.getValue().getStartDegree(), Constants.ARC_DEGREE);
		}
		shapeRenderer.end();
	}
	
	private void drawBall() {
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(gameWorld.getCurrentBall().getColor());
		shapeRenderer.circle(gameWorld.getCurrentBall().getCircle().x, gameWorld.getCurrentBall().getCircle().y, Constants.BALL_RADIUS);
		shapeRenderer.end();
	}
	
	private void drawScore() {
		batch.begin();
		font.setColor(Color.BLACK);
		font.draw(batch, Integer.toString(gameWorld.getScore()), 50, Gdx.graphics.getHeight() - 50);
		batch.end();
	}
	
	private void drawPauseScreen() {
		Assets.pauseScreenStage.act();
		Assets.pauseScreenStage.draw();
	}
	
	private void drawOverScreen() {
		Assets.overScreenStage.act();
		Assets.overScreenStage.draw();
	}
	
	private void drawGameScreen() {
		drawScore();
		drawWheel();
		drawBall();	
		
		Assets.gameScreenStage.act();
		Assets.gameScreenStage.draw();
	}
	
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		GameState currentState = gameWorld.getCurrentState();
		if (currentState.equals(GameState.RUNNING)) {
			Gdx.input.setInputProcessor(Assets.gameScreenStage);
			drawGameScreen();
		} else if (currentState.equals(GameState.PAUSE)){
			Gdx.input.setInputProcessor(Assets.pauseScreenStage);
			drawPauseScreen();
		} else if (currentState.equals(GameState.OVER)) {
			Gdx.input.setInputProcessor(Assets.overScreenStage);
			drawOverScreen();
		} 
	}
}
