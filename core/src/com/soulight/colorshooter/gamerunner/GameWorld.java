package com.soulight.colorshooter.gamerunner;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.soulight.colorshooter.ColorShooter;
import com.soulight.colorshooter.Constants;
import com.soulight.colorshooter.GameState;
import com.soulight.colorshooter.helper.ColorHelper;
import com.soulight.colorshooter.helper.InputHelper;
import com.soulight.colorshooter.model.Ball;
import com.soulight.colorshooter.model.BallPosition;
import com.soulight.colorshooter.model.WheelColorArea;
import com.soulight.colorshooter.resource.Assets;
import com.soulight.colorshooter.screen.GameScreen;
import com.soulight.colorshooter.screen.MenuScreen;

public class GameWorld {
	
	private float rotateTime = 0;
	private int score = 0;
	private int rollingSpeed = 160;
	private int numCycle = 0;
	private float shootingSpeed = 40;
	private float cycleStartDegree = 0;
	
	private boolean isShootTrigger = false;
	private boolean isClockwise = false;
	 
	private List<Ball> listBalls;
	private Ball currentBall;
	private Circle wheel;
	private GameState currentState;

	private Map<Color,WheelColorArea> colorAreaMap;
	
	private Button overMenuButton, overRestartButton;
	private Button pauseMenuButton, pauseResumeButton, pauseRestartButton;
	private Button gamePauseButton;
	
	private List<Button> gameScreenButtons = new ArrayList<Button>();
	
	private ColorShooter game;
	
	public GameWorld() {
		currentBall = getBall(0);
		generateWheel();
		generateButtons();
		currentState = GameState.RUNNING;
		wheel = new Circle(Constants.WHEEL_CENTER_X, Constants.WHEEL_CENTER_Y,Constants.WHEEL_RADIUS);
	}
	
	public void update(float delta) {
		if (currentState.equals(GameState.RUNNING)) {
			updateRunning(delta);
		}
	}
	
	private void updateRunning(float delta) {
		rotateWheel(delta);
		handleDirection();
		
		if (Gdx.input.justTouched()) {
			if (!InputHelper.isButtonTouched(gameScreenButtons)) {
				isShootTrigger = true;
			}
		}
		
		if (isShootTrigger) {
			updateCurrentBallPosition();
		}
			
		if (wheel.overlaps(currentBall.getCircle())) {
			if (isScore()) {
				isClockwise = !isClockwise;
				isShootTrigger = false;
				currentBall = getBall(-1);
				addScore(1);
				if (rollingSpeed < Constants.MAX_ROLLING_SPEED) {
					rollingSpeed += 20;
				}
			} else {
				currentState = GameState.OVER;
				if (score > Assets.getHighScore()) {
					Assets.setHighScore(score);
				}
			}
		}
	}

	public Circle getWheel() {
		return wheel;
	}

	public Ball getCurrentBall() {
		return currentBall;
	}
	
	public int getScore() {
		return score;
	}
	
	public void addScore(int increment) {
		score += increment;
	}

	public Map<Color, WheelColorArea> getColorAreaMap() {
		return colorAreaMap;
	}
	
	public GameState getCurrentState() {
		return currentState;
	}
	
	public void setGame(ColorShooter game) {
		this.game = game;
	}

	private void rotateWheel(float delta) {
		cycleStartDegree += delta * rollingSpeed;
		
		if (isClockwise) {
			rotateTime = 360 - (delta * rollingSpeed);
		} else {
			rotateTime = delta * rollingSpeed;
		}
		
		for (Entry<Color,WheelColorArea> entry : colorAreaMap.entrySet()) {
			WheelColorArea colorArea = entry.getValue();
			
			float startDegree = colorArea.getStartDegree() + rotateTime;
			float endDegree = startDegree + Constants.ARC_DEGREE;
			
			if (startDegree > 360 && endDegree > 360 ) {
				startDegree = startDegree % 360;
				endDegree = endDegree % 360;
			} else if (startDegree < 360 && endDegree > 360) {
				startDegree = startDegree - 360;
				endDegree = endDegree - 360;
			} 
			
			colorArea.setStartDegree(startDegree);
			colorArea.setEndDegree(endDegree);
		}	
	}
	
	private void handleDirection() {
		if (cycleStartDegree >= 360 ) {
			numCycle++;
			cycleStartDegree = 0;
		}
		
		if (numCycle >= Constants.MAX_NUM_CYCLE) {
			isClockwise = !isClockwise;
			numCycle = 0;
		}
	}
	
	private void generateWheel() {
		colorAreaMap = new HashMap<Color,WheelColorArea>();
		colorAreaMap.put(Color.RED, new WheelColorArea(0,Constants.ARC_DEGREE,Color.RED));
		colorAreaMap.put(Color.ORANGE, new WheelColorArea(45,45 + Constants.ARC_DEGREE,Color.ORANGE));
		colorAreaMap.put(Color.YELLOW, new WheelColorArea(90,90 + Constants.ARC_DEGREE,Color.YELLOW));
		colorAreaMap.put(Color.GREEN, new WheelColorArea(135,135 + Constants.ARC_DEGREE,Color.GREEN));
		colorAreaMap.put(Color.BLUE, new WheelColorArea(180,180 + Constants.ARC_DEGREE,Color.BLUE));
		colorAreaMap.put(Color.PURPLE, new WheelColorArea(225,225 + Constants.ARC_DEGREE,Color.PURPLE));
		colorAreaMap.put(Color.PINK, new WheelColorArea(270,270 + Constants.ARC_DEGREE,Color.PINK));
		colorAreaMap.put(Color.MAGENTA, new WheelColorArea(315, 315 + Constants.ARC_DEGREE,Color.MAGENTA));
	}
	
	private void generateButtons() {
		generatePauseScreenButtons();
		generateOverScreenButtons();
		generateGameScreenButtons();
	}
	
	private void generatePauseScreenButtons() {
		pauseRestartButton = new Button(new TextureRegionDrawable(Assets.pauseRestartUp),new TextureRegionDrawable(Assets.pauseRestartDown));
		pauseRestartButton.setSize(Assets.rectangleSizeMap.get("pauseRestartUp").getWidth() * Constants.SCREEN_RATIO, Assets.rectangleSizeMap.get("pauseRestartUp").getHeight() * Constants.SCREEN_RATIO);
		pauseRestartButton.setPosition(50,50);
		pauseRestartButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreen(new GameScreen(game));
			}
		});
		
		pauseMenuButton = new Button(new TextureRegionDrawable(Assets.pauseMenuUp),new TextureRegionDrawable(Assets.pauseMenuDown));
		pauseMenuButton.setSize(Assets.rectangleSizeMap.get("pauseMenuUp").getWidth() * Constants.SCREEN_RATIO, Assets.rectangleSizeMap.get("pauseMenuUp").getHeight() * Constants.SCREEN_RATIO);
		pauseMenuButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreen(new MenuScreen(game));
			}
		});
		
		pauseResumeButton = new Button(new TextureRegionDrawable(Assets.pauseResumeUp),new TextureRegionDrawable(Assets.pauseResumeDown));
		pauseResumeButton.setSize(Assets.rectangleSizeMap.get("pauseResumeUp").getWidth()* Constants.SCREEN_RATIO, Assets.rectangleSizeMap.get("pauseResumeUp").getHeight() * Constants.SCREEN_RATIO);
		pauseResumeButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				currentState = GameState.RUNNING;
			}
		});
		Assets.pauseScreenStage.addActor(pauseMenuButton);
		Assets.pauseScreenStage.addActor(pauseResumeButton);
		Assets.pauseScreenStage.addActor(pauseRestartButton);
	}
	
	private void generateOverScreenButtons() {
		overMenuButton = new Button(new TextureRegionDrawable(Assets.overMenuUp),new TextureRegionDrawable(Assets.overMenuDown));
		overMenuButton.setSize(Assets.rectangleSizeMap.get("overMenuUp").getWidth() * Constants.SCREEN_RATIO, Assets.rectangleSizeMap.get("overMenuUp").getHeight() * Constants.SCREEN_RATIO);
		overMenuButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreen(new MenuScreen(game));
			}
		});
		
		overRestartButton = new Button(new TextureRegionDrawable(Assets.overRestartUp),new TextureRegionDrawable(Assets.overRestartDown));
		overRestartButton.setSize(Assets.rectangleSizeMap.get("overRestartUp").getWidth() * Constants.SCREEN_RATIO, Assets.rectangleSizeMap.get("overRestartUp").getHeight() * Constants.SCREEN_RATIO);
		overRestartButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				game.setScreen(new GameScreen(game));
			}
		});
		Assets.overScreenStage.addActor(overMenuButton);
		Assets.overScreenStage.addActor(overRestartButton);
	}
	
	private void generateGameScreenButtons() {
		gamePauseButton = new Button(new TextureRegionDrawable(Assets.gamePauseUp),new TextureRegionDrawable(Assets.gamePauseDown));
		gamePauseButton.setSize(Assets.rectangleSizeMap.get("gamePauseUp").getWidth(), Assets.rectangleSizeMap.get("gamePauseUp").getHeight());
		gamePauseButton.setPosition(Gdx.graphics.getWidth() - gamePauseButton.getWidth(), Gdx.graphics.getHeight() - gamePauseButton.getHeight());
		gamePauseButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				currentState = GameState.PAUSE;
			}
		});
		
		Assets.gameScreenStage.addActor(gamePauseButton);
		gameScreenButtons.add(gamePauseButton);
	}
	
	private Ball getBall(int index) {
		if (listBalls == null) {
			listBalls = new ArrayList<Ball>();
		}
		if (listBalls.isEmpty()) {
			Ball ballUp = new Ball(new Circle(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()*11/12,Constants.BALL_RADIUS),BallPosition.UP,ColorHelper.getRandomColor());
			Ball ballDown = new Ball(new Circle(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/12,Constants.BALL_RADIUS),BallPosition.DOWN,ColorHelper.getRandomColor());
			Ball ballLeft = new Ball(new Circle(Gdx.graphics.getWidth()/12,Gdx.graphics.getHeight()/2,Constants.BALL_RADIUS),BallPosition.LEFT,ColorHelper.getRandomColor());
			Ball ballRight = new Ball(new Circle(Gdx.graphics.getWidth()*11/12,Gdx.graphics.getHeight()/2,Constants.BALL_RADIUS),BallPosition.RIGHT,ColorHelper.getRandomColor());
			
			listBalls.add(ballDown);
			listBalls.add(ballUp);
			listBalls.add(ballLeft);
			listBalls.add(ballRight);
		}
		if (index < 0 || index >= listBalls.size()) {
			index = (int) (Math.random() * listBalls.size());
		}
		return listBalls.remove(index);
	}
	
	private void updateCurrentBallPosition(){
		float currentBall_x = currentBall.getCircle().x;
		float currentBall_y = currentBall.getCircle().y;
		if (currentBall.getBallPosition().equals(BallPosition.DOWN)) {
			currentBall_y += shootingSpeed;
		} else if (currentBall.getBallPosition().equals(BallPosition.UP)) {
			currentBall_y -= shootingSpeed;
		} else if (currentBall.getBallPosition().equals(BallPosition.LEFT)) {
			currentBall_x += shootingSpeed;
		} else {
			currentBall_x -= shootingSpeed;
		}
		currentBall.setCircle(new Circle(currentBall_x,currentBall_y,Constants.BALL_RADIUS));
	}
	
	
	private boolean isScore() {
		WheelColorArea colorArea = colorAreaMap.get(currentBall.getColor());
		float desireDegree = 0;
		if (currentBall.getBallPosition().equals(BallPosition.UP)) {
			desireDegree = 90;
		} else if (currentBall.getBallPosition().equals(BallPosition.LEFT)) {
			desireDegree = 180;
		} else if (currentBall.getBallPosition().equals(BallPosition.DOWN)) {
			desireDegree = 270;
		} 
		
		if (colorArea.getStartDegree() < desireDegree && colorArea.getEndDegree() > desireDegree) {
			return true;
		}
		
		return false;
	}
}
