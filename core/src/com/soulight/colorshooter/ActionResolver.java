package com.soulight.colorshooter;

import com.soulight.colorshooter.model.RectangleSize;

public interface ActionResolver {

	public boolean isSignedIn();
	
	public void login();
	
	public void showAds();
	
	public RectangleSize getAdSize();
	
	public void getLeaderBoard();
	
	public void submitScore(int score);
	
	public void rate();
	
	public void share();
}
