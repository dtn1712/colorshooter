package com.soulight.colorshooter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;

public class Constants {
	
	public static final String APP_NAME = "ColorShooter";
	
	public static final String SEMICOLON_SEPARATOR = ";";
	public static final String COMMA_SEPARATOR = ",";
	public static final String LINEBREAK_SEPARATOR = "\n";
	public static final String COLON_SEPARATOR = ":";
	public static final String PERIOD_SEPARATOR = ".";
	public static final String EQUAL_SEPARATOR = "=";
	
	public static final String GAME_SCREEN_KEY = "GameScreen";
	public static final String MENU_SCREEN_KEY = "MenuScreen";
	public static final String PAUSE_SCREEN_KEY = "HomeScreen";
	public static final String OVER_SCREEN_KEY = "OverScreen";

	public static final String TEXTURE_IMAGE_PATH = "image/texture1.png";
	public static final String TEXTURE_DATA_PATH = "data/texture.txt";
	public static final String POSITION_DATA_PATH = "data/position.txt";
	public static final String SKIN_PATH = "skins/uiskin.json";
	public static final String FONT_PATH = "fonts/font.ttf";
	
	public static final int DEFAULT_FONT_SIZE = 48;
	public static final int SMALL_SCREEN_FONT_SIZE = 36;
	public static final int BIG_SCREEN_FONT_SIZE = 60;
	
	public static final float WHEEL_CENTER_X = Gdx.graphics.getWidth()/2;
	public static final float WHEEL_CENTER_Y =  Gdx.graphics.getHeight()/2;
	public static final int WHEEL_RADIUS = Gdx.graphics.getWidth() / 6;
	public static final int BALL_RADIUS = WHEEL_RADIUS / 8;
	
	public static final float SCREEN_RATIO = (float)Gdx.graphics.getHeight() / (float)Gdx.graphics.getWidth();
	
	public static final int ARC_DEGREE = 45;
	
	public static final int MAX_ROLLING_SPEED = 400;
	
	public static final int MAX_NUM_CYCLE = 5;
	
	public static final List<Color> COLORS_SET = Collections.unmodifiableList(Arrays.asList(
			Color.RED, 
			Color.BLUE, 
			Color.GREEN, 
			Color.MAGENTA,
			Color.ORANGE,
			Color.PINK,
			Color.PURPLE,
			Color.YELLOW
	));
	
}
