package com.soulight.colorshooter.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.view.View;
import android.graphics.Color;
import android.net.Uri;
import android.widget.RelativeLayout.LayoutParams;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;
import com.google.example.games.basegameutils.GameHelper.GameHelperListener;
import com.soulight.colorshooter.ActionResolver;
import com.soulight.colorshooter.ColorShooter;
import com.soulight.colorshooter.model.RectangleSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class AndroidLauncher extends AndroidApplication implements GameHelperListener, ActionResolver {
	
	private GameHelper gameHelper;
	private InterstitialAd interstitialAd;
	protected AdView adView;	
	protected View gameView;
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
	    cfg.useAccelerometer = false;
	    cfg.useCompass = false;

	    // Do the stuff that initialize() would do for you
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

	    RelativeLayout layout = new RelativeLayout(this);
	    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
	    layout.setLayoutParams(params);

	    AdView admobView = createAdView();
	    layout.addView(admobView);
	    View gameView = createGameView(cfg);
	    layout.addView(gameView);

	    setContentView(layout);
	    startAdvertising(admobView);
	    
	    interstitialAd = new InterstitialAd(this);
	    interstitialAd.setAdUnitId(Constants.AD_UNIT_ID_INTERSTITIAL);
	    interstitialAd.setAdListener(new AdListener() {
	    	@Override
	    	public void onAdLoaded() {
	    		Gdx.app.log(com.soulight.colorshooter.Constants.APP_NAME, "Ad is loaded");
	    	}
	    	@Override
	    	public void onAdClosed() {
	    		Gdx.app.log(com.soulight.colorshooter.Constants.APP_NAME, "Ad is closed");
	    	}
	    });
	
		if (gameHelper == null) {
			gameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
			gameHelper.enableDebugLog(true);
		}
		gameHelper.setup(this);
	}
		
	private AdView createAdView() {
		adView = new AdView(this);
		adView.setAdSize(AdSize.SMART_BANNER);
		adView.setAdUnitId(Constants.AD_UNIT_ID_BANNER);
		adView.setId(12345); // this is an arbitrary id, allows for relative positioning in createGameView()
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
		adView.setLayoutParams(params);
		adView.setBackgroundColor(Color.BLACK);
		return adView;
	}
	  
	private View createGameView(AndroidApplicationConfiguration cfg) {
		gameView = initializeForView(new ColorShooter(this), cfg);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.BELOW, adView.getId());
		gameView.setLayoutParams(params);
		return gameView;	  
	}
	
	private void startAdvertising(AdView adView) {
		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);
	}
	
	@Override
	public void onStart(){
		super.onStart();
		gameHelper.onStart(this);
	}

	@Override
	public void onStop(){
		super.onStop();
		gameHelper.onStop();
	}

	@Override
	public void onActivityResult(int request, int response, Intent data) {
		super.onActivityResult(request, response, data);
		gameHelper.onActivityResult(request, response, data);
	}

	@Override
	public boolean isSignedIn() {
		return gameHelper.isSignedIn();
	}

	@Override
	public void login() {
		try {
			runOnUiThread(new Runnable(){
				public void run() {
					gameHelper.beginUserInitiatedSignIn();
				}
			});
		} catch (final Exception e) {
			Gdx.app.error(com.soulight.colorshooter.Constants.APP_NAME,"Failed to login to game service", e);
		}
	}

	@Override
	public void showAds() {
		try {
			runOnUiThread(new Runnable() {
				public void run() {
					if (interstitialAd.isLoaded()) {
						interstitialAd.show();
		            } else {
						AdRequest interstitialRequest = new AdRequest.Builder().build();
						interstitialAd.loadAd(interstitialRequest);
					}
				}
			});
		} catch (Exception e) {
			Gdx.app.error(com.soulight.colorshooter.Constants.APP_NAME, "Failed to show ads",e);
		}
	}
	
	@Override
	public RectangleSize getAdSize() {
		return null;
	}

	@Override
	public void getLeaderBoard() {
		if (gameHelper.isSignedIn()) {
			startActivityForResult(Games.Leaderboards.getLeaderboardIntent(gameHelper.getApiClient(), Constants.APP_ID), 100);
		} else if (!gameHelper.isConnecting()) {
			login();
		}
	}

	@Override
	public void submitScore(int score) {
		Games.Leaderboards.submitScore(gameHelper.getApiClient(), Constants.APP_ID, score);
	}

	@Override
	public void rate() {
		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.RATE_URL)));
	}

	@Override
	public void share() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onSignInFailed() {
	}

	@Override
	public void onSignInSucceeded() {
	}
}
