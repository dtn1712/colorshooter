package com.soulight.colorshooter.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.soulight.colorshooter.ColorShooter;
import com.soulight.colorshooter.service.ServiceManager;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		ServiceManager serviceManager = new ServiceManager();
		new LwjglApplication(new ColorShooter(serviceManager), config);
	}
}
